#!/usr/bin/env python3

from pathlib import Path
import json
import sys
import hashlib
import logging
import subprocess
import glob
import os
import requests


LOGGER = logging.getLogger(__name__)

fmt = "[%(asctime)s] %(levelname)s: %(message)s"
datefmt = "%H:%M:%S"


formatter = logging.Formatter(fmt, datefmt=datefmt)

log_handler = logging.StreamHandler()
log_handler.setFormatter(formatter)
LOGGER.addHandler(log_handler)
LOGGER.setLevel("DEBUG")

HA_CONFIG_PATH="/data/options.json"
RCLONE_CONFIG_PATH=os.getenv("RCLONE_CONFIG","/data/rclone.conf")
BACKUP_FOLDER="/backup"
SUPERVISOR_API="http://supervisor"
SUPERVISOR_TOKEN=os.getenv("SUPERVISOR_TOKEN")

def read_json_file(file_path):
    LOGGER.debug(f"Reading json file {file_path}")
    with open(file_path, 'r') as myfile:
        data=myfile.read()
    return json.loads(data)

def call_api_supervisor(method, resource):
    LOGGER.debug(f"Calling {method} {resource}")
    headers = {
        "Authorization": f"Bearer {SUPERVISOR_TOKEN}",
        "Content-Type": "application/json"
        }
    if method == "GET":
        LOGGER.debug(f"{SUPERVISOR_API}{resource}")
        LOGGER.debug(headers)
        res = requests.get(f"{SUPERVISOR_API}{resource}", allow_redirects=True, headers=headers)
    elif method == "POST":
        res = requests.post(f"{SUPERVISOR_API}{resource}", allow_redirects=True, headers=headers)
    elif method == "DELETE":
        res = requests.delete(f"{SUPERVISOR_API}{resource}", allow_redirects=True, headers=headers)
    else:
        LOGGER.critical(f"Method {method} not allowed")
        os.exit(1)
    LOGGER.debug(f"Status code {res.status_code}")
    assert res.status_code == 200
    LOGGER.debug(f"Response {res.json()}")
    return res.json()["data"]

def reload_backups():
    call_api_supervisor("POST", "/backups/reload")

def delete_local_backup(slug):
    call_api_supervisor("DELETE", f"/backups/{slug}")

def main():
    LOGGER.info("Starting rclone addon...")
    if Path(HA_CONFIG_PATH).is_file():
        ha_config = read_json_file(HA_CONFIG_PATH)
    else:
        LOGGER.critical(f"File {HA_CONFIG_PATH} not exists.")
        sys.exit(1)
    LOGGER.setLevel(ha_config["log_level"].upper())

    rclone_file_data = "[default]\n"+ ha_config["rclone_config"]
    with open(RCLONE_CONFIG_PATH, 'w') as outfile:
        outfile.write(rclone_file_data)

    local_backups = [os.path.basename(f) for f in glob.glob(f"{BACKUP_FOLDER}/*.tar")]
    LOGGER.info("Local backups:")
    LOGGER.info(local_backups)

    out = subprocess.check_output("rclone tree default:backup-minio-hass --sort-modtime --noindent --level 0 --noreport", shell=True)
    remote_backups = list(filter(lambda x: (x != "/" and x != ""), out.decode().split("\n")))
    LOGGER.info("Remote backups before copy:")
    LOGGER.info(remote_backups)

    # Copy remote backups to local
    backs_sync_bac = []
    if len(local_backups) < ha_config["max_local_backups"]:
        if len(remote_backups) > len(local_backups):
            for rem_back_file in remote_backups:
                if rem_back_file not in local_backups:
                    subprocess.check_call(f"rclone copy --no-traverse default:{ha_config['rclone_copy_dest']}/{rem_back_file} {BACKUP_FOLDER}", shell=True)
                    backs_sync_bac.append(rem_back_file)
    if len(backs_sync_bac) > 0:
        LOGGER.info(f"Copied to local {len(backs_sync_bac)} backups")
        reload_backups()

    subprocess.check_call(f"rclone copy --no-traverse {BACKUP_FOLDER} default:{ha_config['rclone_copy_dest']}", shell=True)

    out = subprocess.check_output("rclone tree default:backup-minio-hass --sort-modtime --noindent --level 0 --noreport", shell=True)
    remote_backups = list(filter(lambda x: (x != "/" and x != ""), out.decode().split("\n")))
    LOGGER.info("Remote backups after copy:")
    LOGGER.info(remote_backups)

    backs_delete = []
    if len(local_backups) > ha_config["max_local_backups"]:
        no_to_delete = len(local_backups) - ha_config["max_local_backups"]
        LOGGER.info(f"Local backups exceed max_local_backups. Deleting last {no_to_delete} backups...")
        for local_backup_file in local_backups[-no_to_delete:]:
            if local_backup_file in remote_backups:
                delete_local_backup(local_backup_file.rstrip(".tar"))
                backs_delete.append(local_backup_file)
    if len(backs_delete) > 0:
        LOGGER.info(f"Deleted local backups {len(backs_delete)}")

    LOGGER.info("Success! Stopping...")


if __name__ == "__main__":
    main()
