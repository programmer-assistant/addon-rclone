# Changelog

# [1.1.0](https://gitlab.com/programmer-assistant/addon-rclone/compare/v1.0.1...v1.1.0) (2021-10-24)


### :bug:

* Fix bug with copy remote to local ([517f569](https://gitlab.com/programmer-assistant/addon-rclone/commit/517f569caa6091c8f1a9170b29b18f615e9c3bbc))

# [1.0.0](https://gitlab.com/programmer-assistant/addon-rclone/compare/v0.1.3...v1.0.0) (2021-10-23)


### :building_construction:

* Rewrite entire plugin to sync files on manual run ([d84d6df](https://gitlab.com/programmer-assistant/addon-rclone/commit/d84d6df5ff5c75b62f71132d7ef6b5e87c4fe758))

## [0.1.3](https://gitlab.com/programmer-assistant/addon-rclone/compare/v0.1.2...v0.1.3) (2021-09-20)


### :fire:

* Fix bug with serving gui ([c9e93b9](https://gitlab.com/programmer-assistant/addon-rclone/commit/c9e93b9b7b0a46e9b54379b15bf6d0c7fd5b0f26))

## [0.1.2](https://gitlab.com/programmer-assistant/addon-rclone/compare/v0.1.1...v0.1.2) (2021-09-20)


### :fire:

* Fix bug with ingress ([87786af](https://gitlab.com/programmer-assistant/addon-rclone/commit/87786af8c34fa0cdef65ee3fa65b4db2a04d5e3a))

## [0.1.1](https://gitlab.com/programmer-assistant/addon-rclone/compare/v0.1.0...v0.1.1) (2021-09-19)


### :fire:

* Fix bug with map ([b8adee3](https://gitlab.com/programmer-assistant/addon-rclone/commit/b8adee3356aea5822db0f70ed6dab212f7f6cc82))

## 0.1.0

* Initial build based on Rclone v1.56.1
