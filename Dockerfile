FROM rclone/rclone:1.56.1 as rclone

FROM programmerassistant/base:3.14

COPY --from=rclone /usr/local/bin/rclone /usr/local/bin/rclone
RUN apk add --no-cache python3 py3-pip \
    && pip install --no-cache-dir requests
COPY rootfs /

ARG BUILD_DATE
ARG BUILD_DESCRIPTION
ARG BUILD_NAME
ARG BUILD_REF
ARG BUILD_VERSION

# Labels
LABEL \
    io.hass.name="${BUILD_NAME}" \
    io.hass.description="${BUILD_DESCRIPTION}" \
    io.hass.arch="multiarch" \
    io.hass.type="addon" \
    io.hass.version=${BUILD_VERSION} \
    maintainer="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.title="${BUILD_NAME}" \
    org.opencontainers.image.description="${BUILD_DESCRIPTION}" \
    org.opencontainers.image.vendor="Programmer Assistant HASS Add-on" \
    org.opencontainers.image.authors="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://addons.programmer-assistant.io" \
    org.opencontainers.image.source="https://gitlab.com/programmer-assistant/addon-rclone" \
    org.opencontainers.image.documentation="https://gitlab.com/programmer-assistant/addon-rclone/-/blob/main/README.md" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BUILD_VERSION}