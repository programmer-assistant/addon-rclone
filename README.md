# Programmer Assistant HASS Add-on: Rclone

![Project Stage][project-stage-shield]
[![License][license-shield]](LICENSE.md)

![Supports aarch64 Architecture][aarch64-shield]
![Supports amd64 Architecture][amd64-shield]
![Supports armv7 Architecture][armv7-shield]
![Supports i386 Architecture][i386-shield]

[![GitLab CI][gitlab-ci-badge]][gitlab-ci]
![Project Maintenance][maintenance-shield]

Rclone add-on by Programmer Assistant add-ons.

## About

[Rclone] is a command line program to manage files on cloud storage. It is a feature rich alternative to cloud vendors' web storage interfaces. Over 40 cloud storage products support rclone including S3 object stores, business & consumer file storage services, as well as standard transfer protocols.

[:books: Read the full add-on documentation][docs]

---

Do you like this? Give me a reason to keep doing it and

[![Buy me a coffee!][bmc-shield]][bmc]

## Support

Got questions?

[Open an issue here][issue] on GitLab.

## Contributing

This is an active open-source project. We are always open to people who want to
use the code or contribute to it.

We have set up a separate document containing our
[contribution guidelines](CONTRIBUTING.md).

Thank you for being involved!

## Authors & contributors

The original setup of this repository is by [Dawid Rycerz][knightdave].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## We have got some Home Assistant add-ons for you

Want some more functionality to your Home Assistant instance?

We have created multiple add-ons for Home Assistant. For a full list, check out
our [Add-on Repository][repository].

## License

MIT License

Copyright (c) 2021-2021 Dawid Rycerz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[Rclone]: https://rclone.org
[project-stage-shield]: https://img.shields.io/badge/project%20stage-experimental-blue.svg
[license-shield]: https://img.shields.io/badge/License-MIT-yellow.svg
[aarch64-shield]: https://img.shields.io/badge/aarch64-yes-green.svg
[amd64-shield]: https://img.shields.io/badge/amd64-yes-green.svg
[armhf-shield]: https://img.shields.io/badge/armhf-yes-green.svg
[armv7-shield]: https://img.shields.io/badge/armv7-yes-green.svg
[i386-shield]: https://img.shields.io/badge/i386-yes-green.svg
[gitlab-ci-badge]: https://gitlab.com/programmer-assistant/addon-rclone/badges/main/pipeline.svg
[gitlab-ci]: https://gitlab.com/programmer-assistant/addon-rclone/-/pipelines
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[docs]: https://gitlab.com/programmer-assistant/addon-rclone/-/blob/main/DOCS.md
[bmc-shield]: https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png
[bmc]: https://buymeacoff.ee/rycerz
[issue]: https://gitlab.com/programmer-assistant/addon-rclone/-/issues
[knightdave]: https://gitlab.com/knightdave
[contributors]: https://gitlab.com/programmer-assistant/addon-rclone/-/graphs/main
[repository]: https://addons.programmer-assistant.io

<!--[project-stage-shield]: https://img.shields.io/badge/project%20stage-production%20ready-brightgreen.svg -->
